# EASY
# require 'byebug'
# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  copy = str.dup.chars
  copy.each_with_index { |ch, idx| copy.delete_at(idx) if ch.downcase == ch }
  copy.join('')
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  len = str.length
  (len%2==1 ? str[len/2] : str[len/2-1,2])
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  num=0
  str.each_char { |ch| num+=1 if VOWELS.include?(ch) }
  num
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  result=1
  1.upto(num) { |x| result *= x }
  result
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  arr_length = arr.length
  result_string=""
  arr.each_with_index do |el, idx|
    result_string += el
    if idx < arr.length-1
      result_string += separator
    end
  end
  result_string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  result_str = ''
  str.each_char.with_index do |ch, idx|
    if idx.odd?
      result_str += ch.upcase
    else
      result_str += ch.downcase
    end
  end
  result_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str_arr = str.split(' ')
  result_arr = []
  str_arr.each { |word| (word.length >=5 ? result_arr << word.reverse : result_arr << word) }
  result_arr.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result = []
  (1..n).each do |x|
    if x%3==0 && x%5==0
      result << "fizzbuzz"
    elsif x%3==0
      result << "fizz"
    elsif x%5==0
      result << "buzz"
    else
      result << x
    end
  end
  result
end

# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  result = []
  arr.each { |x| result.unshift(x) }
  result
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  (2...num).each { |x| return false if num % x == 0 }
  return true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  result = []
  (1..num).each { |x| result << x if num % x == 0 }
  result
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  result = []
  (1..num).each { |x| result << x if num % x == 0 && prime?(x) }
  result
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end

# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  even_arr = []
  odd_arr = []
  arr.each do |x|
    even_arr << x if x.even?
    odd_arr << x if x.odd?
  end
  return even_arr[0] if even_arr.length == 1
  return odd_arr[0] if odd_arr.length == 1
end
